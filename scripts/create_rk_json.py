import json, random
from uuid import uuid4


def create_module_runkey(rk: dict, module_dict: list[dict]):
    modules = []
    size = rk["metadata"]["nMods"]
    for i in range(size):
        module = module_dict[i % len(module_dict)]
        metadata = {
            "serial": module["serial"],
            "position": i + 1,
            "paths": module["paths"],
        }
        child = {"type": "module", "name": f"module_{i}", "metadata": metadata}
        modules.append(child)

    # random.shuffle(modules)
    rk["objects"] = [{"type": "modules", "children": modules}]

    with open(f'scripts/{rk["name"]}.json', "w") as file:
        json.dump(rk, file, indent=4)


def create():
    modules = [
        {
            "serial": "20UPGM22110471",
            "paths": [
                {"type": "as", "path": "20UPGM22110471/Jig1/010464"},
                {"type": "ds", "path": "20UPGM22110471/Jig1/010463"},
                {"type": "ts", "path": "20UPGM22110471/Jig1/010461"},
            ],
        },
        {
            "serial": "20UPGR92110514",
            "paths": [
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig1/000214"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig1/000213"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig1/000215"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig2/000210"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig2/000209"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig2/000211"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig3/000205"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig3/000203"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig3/000207"},
                {"type": "as", "path": "20UPGR92110514/DQ28_Jig4/000200"},
                {"type": "ds", "path": "20UPGR92110514/DQ28_Jig4/000199"},
                {"type": "ts", "path": "20UPGR92110514/DQ28_Jig4/000201"},
            ],
        },
    ]

    with open("scripts/LLS_OB_CERN_collection.json", "r") as file:
        data = json.load(file)

    rks = data["list"]

    for rk in rks:
        create_module_runkey(rk, modules)


if __name__ == "__main__":
    create()
