from pyconfigdb import ConfigDB
from uuid import uuid4
import json


def main():
    key = "demi/default/itk-demo-configdb/api/url"
    configdb = ConfigDB(key)

    itsdaq_number = 5

    configdb.stage_create(
        data={"type": "currentSide"}, name="my_runkey"
    )  # Add a more complex runkey here
    configdb.stage_commit("my_runkey", "my_committed_runkey")

    # Get the current side ID (after commit)
    # This depends on runkey structure
    runkey = configdb.tag_tree("my_committed_runkey", stage=False)
    currentSideID = runkey["objects"][0]["id"]

    hccPayloads = [
        {
            "type": "itsdaq_number",
            "data": json.dumps({"number": str(itsdaq_number)}),
            "meta": True,
        }
    ]
    hccNode = configdb.add_node(
        type="HCC",
        payloads=hccPayloads,
        parents=[{"id": currentSideID}],
        children=[],
        stage=False,
    )

    searchDict = {"number": str(itsdaq_number)}
    hccs = configdb.search(
        "my_committed_runkey",
        config_type="itsdaq_number",
        object_type="HCC",
        search_dict=searchDict,
    )
    hccs2 = configdb.search(
        currentSideID,
        config_type="itsdaq_number",
        object_type="HCC",
        search_dict=searchDict,
    )
    # Both runkey name or ID can be used, config_type and object_type are optional (but will speed up the query, so recommended to send them if known)

    if hccs == hccs2:
        print(hccs)
    else:
        print("???")


if __name__ == "__main__":
    main()
