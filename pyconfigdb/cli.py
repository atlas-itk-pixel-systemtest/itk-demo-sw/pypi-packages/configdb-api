import click, os, functools
from pyconfigdb.configdb import ConfigDB
from pyconfigdb.exceptions import DBException


def errorhandling_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except FileNotFoundError as e:
            click.echo(e)
            click.echo(
                "Make sure the given directory exists and (if running pyconfigdb in a container) is mounted to your specified path."
            )
            exit
        except DBException as e:
            click.echo(e)
            exit

    return wrapper


class InputError(Exception):
    def __init__(self, msg):
        self.title = "Input Error"
        self.msg = msg
        super().__init__(f"{self.title} - {self.msg}")


@click.group("pyconfigdb")
def pyconfigdb():
    pass


@pyconfigdb.command()
@click.argument("export_path", type=str)
@click.argument("runkey_name", type=str)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@click.option(
    "-b",
    "--backend",
    is_flag=True,
    default=True,
    help="Defines whether to read from stage or backend table",
)
@errorhandling_decorator
def export_rk(export_path, runkey_name, key, backend, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    configdb.export_runkey(export_path, runkey_name, stage=backend)


@pyconfigdb.command()
@click.argument("import_path", type=str)
@click.argument("runkey_name", type=str)
@click.option(
    "-a",
    "--author",
    type=str,
    default="",
    help="Author of tag",
)
@click.option(
    "-c",
    "--comment",
    type=str,
    default="",
    help="Comment of tag",
)
@click.option(
    "--commit",
    type=bool,
    is_flag=True,
    default=False,
    help="Defines whether to commit the imported runkey or leave it in the staging area",
)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@errorhandling_decorator
def import_rk(import_path, runkey_name, author, comment, commit, key, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    configdb.import_runkey(import_path, runkey_name, author, comment, commit)


@pyconfigdb.command()
@click.argument("identifier", type=str)
@click.argument("new_name", type=str)
@click.option(
    "-t",
    "--type",
    type=str,
    default="runkey",
    help="Type of tag",
)
@click.option(
    "-a",
    "--author",
    type=str,
    default="",
    help="Author of tag",
)
@click.option(
    "-c",
    "--comment",
    type=str,
    default="",
    help="Comment of tag",
)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@errorhandling_decorator
def clone(identifier, new_name, type, author, comment, key, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    configdb.stage_clone(
        identifier, new_name, type=type, author=author, comment=comment
    )


@pyconfigdb.command()
@click.argument("identifier", type=str)
@click.argument("new_name", type=str)
@click.option(
    "-t",
    "--type",
    type=str,
    default="runkey",
    help="Type of tag",
)
@click.option(
    "-a",
    "--author",
    type=str,
    default="",
    help="Author of tag",
)
@click.option(
    "-c",
    "--comment",
    type=str,
    default="",
    help="Comment of tag",
)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@errorhandling_decorator
def commit(identifier, new_name, type, author, comment, key, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    configdb.stage_commit(
        identifier, new_name, type=type, author=author, comment=comment
    )


@pyconfigdb.command()
@click.argument("identifier", type=str)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@errorhandling_decorator
def delete(identifier, key, sr_url):
    try:
        configdb = ConfigDB(key, srUrl=sr_url)
        configdb.stage_delete(identifier)
    except DBException as e:
        click.echo(e)


@pyconfigdb.command()
@click.argument("module_type", type=str)
@click.argument("runkey_name", type=str)
@click.option(
    "-f",
    "--filter",
    type=str,
    default="",
    help="Filter for the scan_config type",
)
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@errorhandling_decorator
def import_scan(module_type, runkey_name, filter, key, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    configdb.import_scan_configs(
        module_type=module_type, runkey_name=runkey_name, filter=filter
    )
    click.echo(f"Committed scan configs for {module_type} with name {runkey_name}.")


@pyconfigdb.command()
@click.option(
    "-k",
    "--key",
    type=str,
    default=os.environ.get("KEY", "demi/default/itk-demo-configdb/api/url"),
    help="Service registry key to configdb",
)
@click.option(
    "-s",
    "--sr_url",
    type=str,
    default=os.environ.get("SR_URL", "http://localhost:5111/api/"),
    help="URL of the Service registry",
)
@click.option(
    "-t",
    "--type",
    type=str,
    default="runkey",
    help="types to read from tag table",
)
@click.option(
    "-b",
    "--backend",
    is_flag=True,
    default=True,
    help="Defines whether to read from stage or backend table",
)
@errorhandling_decorator
def runkeys(type, backend, key, sr_url):
    configdb = ConfigDB(key, srUrl=sr_url)
    tags = configdb.read_all(
        "tag", filter=type, order_by="time", asc=False, stage=backend
    )
    for tag in tags:
        click.echo(f"{tag['name']} - {tag['author']} - {tag['comment']}")


def main():
    pyconfigdb(prog_name="pyconfigdb")


if __name__ == "__main__":
    main()
