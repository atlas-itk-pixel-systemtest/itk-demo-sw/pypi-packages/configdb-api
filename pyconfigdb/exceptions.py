from requests import JSONDecodeError
from rcf_response.exceptions import RcfException
from pyregistry.exceptions import SRConnectionError

class DBException(RcfException):
    def __init__(self, msg, title="DB Error", status=500, instance=None, ext=None):
        super().__init__(status, title, msg, instance=instance, ext=ext)

class UndefinedError(DBException):
    def __init__(self, fct_name):
        msg = f"Function '{fct_name}' has not been defined for this DB implementation."
        super().__init__(msg, title="DB Function Undefined")


class DBParameterError(DBException):
    def __init__(self, msg):
        title = "DBParameterError"
        super().__init__(msg, title=title, status=400)


class ConfigDBResponseError(DBException):
    def __init__(self, response, default_msg):
        status = response.status_code
        title = "ConfigDBError"
        try:
            data = response.json()
            title += f": {data}"
            msg = f"{default_msg} ConfigDB response: {data['detail']} ({response.url})"
            ext = {"configdb-response": data}
            super().__init__(msg, title=title, status=status, ext=ext)
        except (JSONDecodeError, KeyError):
            super().__init__(default_msg, title=title, status=status)


class ConfigDBConnectionError(DBException):
    def __init__(self, dbUrl):
        title = "ConfigDBError"
        msg = f"No connection could be established to the Config DB at URL {dbUrl}."
        super().__init__(msg, title=title, status=404)


class FSError(DBException):
    def __init__(self, msg):
        title = "FilesystemError"
        super().__init__(msg, title=title, status=400)
