from pyconfigdb import ConfigDB
from dataclass_example.data_classes import (
    TagModel,
    ObjectModel,
    PayloadModel,
)

key = "demi/default/itk-demo-configdb/api/url"
configdb = ConfigDB(key)


def dict_method():
    tree = {
        "type": "root",
        "payloads": [{"data": "root_node payload", "type": "config", "name": "test"}],
        "children": [
            {
                "type": "felix",
                "payloads": [
                    {"data": "payload_felix01", "type": "felix", "name": "test"},
                ],
                "children": [
                    {
                        "type": "lpgbt",
                        "payloads": [
                            {"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}
                        ],
                        "children": [
                            {
                                "type": "frontend",
                                "payloads": [
                                    {
                                        "data": "payload_fe01",
                                        "type": "frontend",
                                        "name": "test",
                                    }
                                ],
                            },
                        ],
                    }
                ],
            }
        ],
    }

    configdb.stage_create(data=tree, name="stage_tree")
    configdb.stage_commit(identifier="stage_tree", name="committed_tree")


def dataclass_method():
    # It is possible to create a tree structure using dataclasses (at once)
    lpgbt = ObjectModel(
        type="lpgbt",
        payloads=[PayloadModel(data="payload_lpgbt01", type="lpgbt", name="test")],
        children=[
            ObjectModel(
                type="frontend",
                payloads=[
                    PayloadModel(data="payload_fe01", type="frontend", name="test")
                ],
                children=[],
            )
        ],
    )
    # or one after each other
    felix = ObjectModel(
        type="felix",
        payloads=[PayloadModel(data="payload_felix01", type="felix", name="test")],
        children=[lpgbt],
    )
    tree = ObjectModel(
        type="root",
        payloads=[PayloadModel(data="root_node payload", type="config", name="test")],
        children=[felix],
    )

    print(tree.model_dump())

    configdb.stage_create(data=tree.model_dump(), name="stage_tree")
    configdb.stage_commit(identifier="stage_tree", name="committed_tree")


if __name__ == "__main__":
    # dict_method()
    dataclass_method()
