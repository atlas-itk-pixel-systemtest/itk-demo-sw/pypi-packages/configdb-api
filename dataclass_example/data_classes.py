from pydantic import BaseModel, Field, AfterValidator
from typing_extensions import Annotated
from typing import Union, ForwardRef, List, Optional
from uuid import uuid4

ObjectModel = ForwardRef("ObjectModel")
ObjectModel = ForwardRef("ObjectModel")
ReuseConnectionModel = ForwardRef("ReuseConnectionModel")
ReuseModel = ForwardRef("ReuseModel")
PayloadModel = ForwardRef("PayloadModel")
ConnectionModel = ForwardRef("ConnectionModel")


def convert_id(v):
    if not isinstance(v, ConnectionModel):
        con = ConnectionModel(id=v)
        return con
    else:
        return v


def convert_ids(v):
    for i, value in enumerate(v):
        if not isinstance(value, ConnectionModel):
            v[i] = ConnectionModel(id=value)
    return v


IDFieldDef = Field(
    min_length=32,
    max_length=36,
    pattern="^[0-9a-f]{8}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{4}-?[0-9a-f]{12}$",
    description="uuid of the dataset as a string without dashes",
    examples=[uuid4().hex],
)
NameFieldDef = Field(
    description="name of the tag",
    examples=["my_runkey_name"],
)

OptionalIDField = Annotated[Optional[str], IDFieldDef]
IDField = Annotated[str, IDFieldDef]

ConnectionField = Annotated[
    Union[ConnectionModel, IDField],
    AfterValidator(convert_id),
    Field(description="connection to another object"),
]
ViewField = Annotated[
    int,
    Field(
        description="view of the tree (e.g.: 1 for default, 2 for stale, 3 for both)"
    ),
]
PayloadListField = Annotated[List[IDField], Field(description="list of payload UUIDs")]
ObjectListField = Annotated[List[IDField], Field(description="list of object UUIDs")]
PayloadReuseField = Annotated[
    List[Union[ReuseModel, PayloadModel]],
    Field(
        description="list of payload datasets with option to reuse existing payloads"
    ),
]
ObjectReuseField = Annotated[
    List[Union[ReuseConnectionModel, ObjectModel]],
    Field(description="list of object datasets with option to reuse existing object"),
]
ChildrenConnField = Annotated[
    List[ConnectionField], Field(description="list of children connections")
]
PayloadIDListField = Annotated[
    List[Union[PayloadModel, IDField]],
    Field(description="list of payload datasets or uuids"),
]
TagTypeField = Annotated[str, Field(description="type of the tag", examples=["runkey"])]
PDBCodeField = Annotated[str, Field(description="access code for the pdb")]


class ConnectionModel(BaseModel):
    id: IDField
    view: ViewField = 1


class PayloadModel(BaseModel):
    id: OptionalIDField = None
    name: Annotated[
        str,
        Field(
            description="name of the payload",
            examples=["fe_config.json"],
        ),
    ] = None
    type: Annotated[
        str,
        Field(
            description="type of the payload",
            examples=["configuration"],
        ),
    ]
    data: Annotated[
        str,
        Field(
            description="data of the payload",
            examples=["payload_data as string"],
        ),
    ] = ""
    meta: Annotated[
        bool,
        Field(
            description="save payload as searchable metadata",
        ),
    ] = False


class PayloadIDModel(PayloadModel):
    id: IDField


class ObjectModel(BaseModel):
    id: OptionalIDField = None
    type: Annotated[
        str,
        Field(
            description="type of the object",
            examples=["frontend"],
        ),
    ]
    children: ObjectReuseField = []
    payloads: PayloadReuseField = []


class ReuseModel(BaseModel):
    reuse_id: IDField


class ReuseConnectionModel(ReuseModel):
    view: ViewField = 1
    reuse_id: IDField


class TagModel(BaseModel):
    id: OptionalIDField = None
    name: Annotated[Optional[str], NameFieldDef]
    author: Annotated[
        str,
        Field(
            description="name of the author of the tag",
            examples=["Max Mustermann"],
        ),
    ] = None
    comment: Annotated[
        str,
        Field(
            description="comment for the tag",
            examples=["comment text"],
        ),
    ] = None
    data: ObjectModel
    type: TagTypeField = "runkey"
    payloads: PayloadListField = []
